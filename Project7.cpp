﻿#include <iostream>
#include <string>

using namespace std;

class Players
{
private:

    string name;
    int point = 0;
    int count = 0;    
    bool result = true;

public:

    Players(int _count) : count(_count)
    {}

    void GetPlayers()
    {
        // Создание массива
        string** players = new string *[count];

        for (int i = 0; i < count; i++)
        {
            players[i] = new string[2];
        }

        // Заполнение массива
        for (int i = 0; i < count; i++)
        {
            cout << "Введите имя игрока: ";
            cin >> name;

            players[i][0] = name;

            for (int j = 1; j < 2; j++)
            {                
                cout << "Задайте количество набранных игроком очков: ";
                cin >> point;

                players[i][j] = point;
            }
        } 
        
        int temp;

        for (int i = 0; i < count; ++i)
        {
            for (int j = 0; j < count - 1; ++j)
            {
                for (int k = 0; k < i - 1; k++)
                {
                    if (players[i][j] > players[i][j + 1])
                    {
                        temp = stoi(players[i][j]);
                        players[k][j] = players[i][j];
                        players[i][j] = temp;
                    }
                }
            }
        }

        // Вывод массива
        for (int i = 1; i < count; i++)
        {
            cout << endl;
            for (int j = 0; j < 2; j++)
            {
                cout << players[i][j] << "\t";
            }
        }

        delete players;
        players = nullptr;
    }

};

int main()
{
    setlocale(LC_ALL, "RU");

    int count;

    cout << "Задайте количество игроков: ";
    cin >> count;
    
    Players players(count);

    players.GetPlayers();

    return 0;
}